import React, {Component} from 'react';
import NodeAttr from './nodeAttrComponent';
import uuid from 'react-uuid';
import RenderNodeTree from './compactNodeComponent';
import { node } from 'prop-types';
import RenderChildNode from './renderChildNode';
import {postAction} from './actionComponent';
import axios from 'axios';
import '../assets/css/style.css';

export default class IvrNode extends Component {

    constructor() {
        super();
        this.getNodeIndexById = this.getNodeIndexById.bind(this, true);
        this.getChildrens = this.getChildrens.bind(this, true);
    }

    state = {
        prepareIvrData: [],
        prepareChildNaode: [],
        ivrLabel: 'Ivr flow listing..',
        nodeatrrshow: 'none',
        showModal: false,
        formNodeData: [],
        setformData: {},
        is_edit: false,
        mapper: {},
        indexBucket: [],
        extrafield: [],
        node_type : {},
        par_id: ''
    }

    

    postAction = () => {
        let data;
    data =  axios.get(`http://127.0.0.1:8000/get_node_type`)
      .then(res => {
        //   let data=JSON.parse(res.data);

          this.setState({showModal:true, node_type:res.data.data, setformData:{parent_id:this.state.par_id}})
        return res.data;
      });

      return data;
    }

    get_req_setng = (r_id) => {
        let data = {}
        let s_data = new FormData();
        s_data.set('id', r_id);
        data =  axios.post(`http://127.0.0.1:8000/get_req_field`, s_data)
        .then(res => {
            this.setState({showModal:true, extrafield:res.data.data})
            return res.data;
        });     
        return data;   

    }

    addNewNode = () => {
        // const node_type = postAction();
        let node_type = this.postAction();
        // let req_stngs = this.get_req_setng();
        

        // this.setState({showModal:true, node_type:node_type.data.data, extrafield:req_stngs.data.data});
    }

    handleModalHide = () => {
        this.setState({showModal:false});
    }

    
    removeNode = (nodeId) => {
        let prepareIvrData = this.state.prepareIvrData.filter(nodeData=>(nodeData.id !== nodeId));
        this.setState({prepareIvrData:prepareIvrData});
    }
    
    editNode = (nodeId) => {
        let prepareIvrData = this.state.prepareIvrData.filter(nodeData=>(nodeData.id === nodeId));
        this.setState({showModal:true, setformData: prepareIvrData[0], is_edit:true});
    }

    getNodeIndexById = (n_id) => {
        // debugger;
        return this.state.prepareIvrData.findIndex(nodeD=>(nodeD.id == n_id));
    }


    getChildrens = (childNode) => {
        if(childNode.length){
        childNode.map(function(key){
            this.state.indexBucket.push(this.getNodeIndexById(key));
            if(this.state.mapper.key.length){
                return this.getChildrens(this.state.mapper.key)
            }
        })}
    }
    
    
    callFormater = () => {
        let mapper = this.state.mapper;
        let prepareIvrData = [];
        // debugger;
        var root_key = Object.keys(this.state.mapper);
        for(let i=0; i<root_key.length; i++){
        }
        

    }
    

    saveEditNode = (event) => {
        let nodeData = {};
        let formData = new FormData(event.target.form); 
        for(var pair of formData.entries()) {
            nodeData[pair[0]] = pair[1];
        }
        let nodeIndex = this.state.prepareIvrData.findIndex(nodeD=>(nodeD.id === nodeData.id));
        nodeData['childNode'] = this.state.prepareIvrData[nodeIndex].childNode; 
        
        // nodeData['childNode'] = this.state.prepareIvrData[]
        let prepareIvrData = this.state.prepareIvrData;
        prepareIvrData[nodeIndex] = nodeData;
        // this.callFormater(this);
        
        this.setState({prepareIvrData, is_edit:false, showModal:false, setformData:{}});
        event.preventDefault();
    }
    // componentDidMount(){
    //     if(this.state.prepareIvrData.length){
    //         this.callFormater();
    //     }
    // }
    componentDidMount(){
        if(this.state.prepareIvrData.length){
            this.callFormater();
        }
    }
    // shouldComponentUpdate()
    savenodeInDb = (n_data) => {
        let s_data = new FormData();
        s_data.set('data', JSON.stringify(this.state.prepareIvrData));
        axios.post(`http://127.0.0.1:8000/save_node`, s_data)
        .then(res => {
            return res.data;
        }); 

    }

    handleModalOnSave = (event) => {
        let nodeData = {};
        let formData = new FormData(event.target.form);
        for(var pair of formData.entries()) {
            nodeData[pair[0]] = pair[1];
         }
         nodeData['childNode'] = []
         if(nodeData.parent_id){
            this.state.mapper[nodeData.id]=[]
            // this.state.prepareIvrData.push(nodeData);
             this.state.mapper[nodeData.parent_id].push(nodeData.id);
            //  debugger;
             let p_index = this.state.prepareIvrData.findIndex(x=> x.id == nodeData.parent_id);
             this.state.prepareIvrData.splice(p_index+1, 0, nodeData)
         }else{
        this.state.mapper[nodeData.id]=[]
         this.state.prepareIvrData.push(nodeData);
         }
         // this.savenodeInDb(nodeData);
         this.setState({prepareIvrData:this.state.prepareIvrData, showModal:false, setformData:{}, formNodeData:[], mapper:this.state.mapper, par_id:nodeData.id});
        event.preventDefault();
    }

    saveBranchNode = (event) => {
        event.preventDefault();
    }

   

    addBranchNode = (nodeId) => {
        this.setState({showModal:true, setformData: {parent_id:nodeId}});
    }

    handleReqField = (event) => {
        this.get_req_setng(event.target.value);
    }

    render() {
        // this.callFormater(this);
        return(
            <React.Fragment>
                <div className="container">
                    {/* {this.fetchIvrLabel} */}
                    <span className="ivr-label">{this.state.ivrLabel}</span><br/>
                    <button className="btn btn-primary" onClick={this.addNewNode} >Add New Node </button>


                    <NodeAttr extrafield={this.state.extrafield} showModal={this.state.showModal} onTypeChange={this.handleReqField}  nodeId={this.state.is_edit? this.state.setformData.id :uuid()} handleModalHide={this.handleModalHide} onSave={this.state.is_edit?this.saveEditNode:this.handleModalOnSave} nodeData={this.state.setformData} parentId={this.state.setformData.parent_id} node_type={this.state.node_type}/> 

                    {/* <NodeAttr /> */}

                    {/* compact node render logic  */}
                    {this.state.prepareIvrData.map(nodeData => (
                        <div>
                            <RenderNodeTree nodeDetails={nodeData} key={nodeData.id} OnDelete={()=>this.removeNode(nodeData.id)} onEdit={() => this.editNode(nodeData.id)} onBranchNode={() => this.addBranchNode(nodeData.id)} /> 

                        </div>
                    ))}
                     <button className="btn btn-primary" onClick={this.savenodeInDb} >Load Ivr </button>

                </div>
            </React.Fragment>
        );
    }
}
import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';



export default class NodeAttr extends Component {
    
    state = {
        options: this.props.node_type,
    }
    render() {
        console.log(this.props.node_type);
        return(
            <React.Fragment>
                <div className="ivr-node-attr">
                  
                 
                    <Modal
                        show={this.props.showModal}
                        onHide={this.props.handleModalHide}
                        dialogClassName="modal-90w"
                        aria-labelledby="example-custom-modal-styling-title"
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="example-custom-modal-styling-title">
                                Ivr Node
                            </Modal.Title>

                        </Modal.Header>

                        <Modal.Body>
                        
                            <Form className="a" id="ivr-node">
                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text id="basic-addon1">Node Name</InputGroup.Text>
                                    </InputGroup.Prepend>

                                    <FormControl
                                        type=''
                                        name='name'
                                        defaultValue={this.props.nodeData.name}
                                        placeholder ="Enter node name"
                                        aria-label="Username"
                                        aria-describedby="basic-addon1"
                                    />
                                </InputGroup>

                                <Form.Group controlId="exampleForm.ControlSelect1">
                                    <Form.Label>Node Type</Form.Label> 
                                    {/* {this.props.node_type} */}
                                    {/* {this.state.options.map(data=>(
                                        <div>
                                        <h1>data.name</h1></div>
                                    ))} */}
                                    <Form.Control as="select" onChange={this.props.onTypeChange} name="node_type" defaultValue={this.props.nodeData.node_type}>
                                    // <option>Please Select</option>
                                    {
                                        (this.props.node_type.length) ?
                                        this.props.node_type.map(data=>(
                                                <option value={data.key} >{data.name}</option>
                                            )):''
                                            
                                        }
                                        </Form.Control>
                                        {/* <h1>okok</h1>
                                        <option value={2}>TTS Play</option>
                                        <option value={3}>Play Menu</option>
                                        <option value={4}>PLay TTS Menu</option> */}
                                </Form.Group>
                                {
                                    (this.props.extrafield.length)?
                                    this.props.extrafield.map(data =>(

                                            <InputGroup className="mb-3">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text id="basic-addon1">{data.name}</InputGroup.Text>
                                                </InputGroup.Prepend>

                                                <FormControl
                                                    defaultValue={data.value}
                                                    name={data.name}
                                                    placeholder="Response"
                                                    aria-label="Response"
                                                    aria-describedby="basic-addon1"
                                                />
                                            </InputGroup>
                                    )): ''
                                }

                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text id="basic-addon1">Response</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        defaultValue={this.props.nodeData.response}
                                        name="response"
                                        placeholder="Response"
                                        aria-label="Response"
                                        aria-describedby="basic-addon1"
                                    />
                                </InputGroup>
                                
                                <InputGroup className="mb-3">
                                    <FormControl
                                        name="id"
                                        placeholder=""
                                        aria-label=""
                                        type='hidden'
                                        value={this.props.nodeId}
                                        aria-describedby="basic-addon1"
                                    />
                                </InputGroup>

                                <InputGroup className="mb-3">
                                    <FormControl
                                        name="parent_id"
                                        placeholder=""
                                        aria-label=""
                                        type='hidden'
                                        value={this.props.parentId}
                                        aria-describedby="basic-addon1"
                                    />
                                </InputGroup>

                                <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text id="basic-addon1">Key</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        name="key"
                                        placeholder="Key"
                                        aria-label="Key"
                                        aria-describedby="basic-addon1"
                                    />
                                </InputGroup>
                               

                                <ButtonToolbar>
                                    <input className="btn btn-primary" type="submit" onClick={this.props.onSave} value="save data"/>
                                    <Button variant="secondary" onClick={this.props.handleModalHide}> Close </Button>
                                </ButtonToolbar>
                            </Form>
                        </Modal.Body>
                    </Modal>
                </div>
            </React.Fragment>
        );
    }
}
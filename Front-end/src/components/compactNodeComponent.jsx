import React, {Component} from 'react';
import { node } from 'prop-types';

// responsible for rendering ivr node tree
export default class RenderNodeTree extends Component {
    handleBranchNode = () => {
        let nodeButton = '';
        console.log("------------");
        console.log(this.props.nodeDetails.node_type);
        if(this.props.nodeDetails.node_type === 3){
            nodeButton = '<button>Add branch Node</button>';
            return nodeButton;
        }
        return nodeButton;
    }
    render () {
        // console.log(JSON.stringify(this.props, null, 2));
        let c_node_class = "table";
        if(this.props.nodeDetails.node_type==13 || this.props.nodeDetails.node_type==14){
            c_node_class = "table";
        }
        const nodebtn = this.handleBranchNode;
        return(
            <React.Fragment>
                <div>
                    <table border="2" className={c_node_class} >
                        <tbody>
                            <tr>
                                <th>{this.props.nodeDetails.id}</th>
                                <th>{this.props.nodeDetails.name}</th>
                                <th>{this.props.nodeDetails.node_type}</th>
                                <th>{this.props.nodeDetails.parent_id}</th>
                                <th><button className="btn-success" onClick={this.props.onEdit}>Edit</button></th>
                                <th><button className="btn-danger" onClick={this.props.OnDelete}>Delete</button></th>
                                {(this.props.nodeDetails.node_type==13 || this.props.nodeDetails.node_type==14) && <th><button className="btn-info" onClick={this.props.onBranchNode}>Add branch Node</button></th>}
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </React.Fragment>
        );
    }
}
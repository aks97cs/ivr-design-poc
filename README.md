# Ivr Design POC


## Folder Structure
### Backend :  
Responsible for backend api intergrations
#### To run appilication
 Dependencies:
 Django v3.0.1
 Python 3.6.9
 mysql db setup

 ### installation
 pip3 install django==3.0.1
 pip3 install django-serializer

 #### Database setup
 create database with same username and password
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ivrnode',
        'HOST': '127.0.0.1',
        'USER': 'ivrnode',
        'PASSWORD': '123',
        'PORT': ''
    }

 1. go to folder location Backend/
 2. RUN python3 manage.py makemigrations
 3. RUN python3 manage.py migrate
 4. RUN python3 manage.py runserver

### Front-end
Dependencies

node v13.3.0 

### To run application
1. go to application folder name Fron-end
2. run command "npm install"
3. run npm start

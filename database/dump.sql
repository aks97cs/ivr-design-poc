-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: ivrnode
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add node type',7,'add_nodetype'),(26,'Can change node type',7,'change_nodetype'),(27,'Can delete node type',7,'delete_nodetype'),(28,'Can view node type',7,'view_nodetype'),(29,'Can add node setting',8,'add_nodesetting'),(30,'Can change node setting',8,'change_nodesetting'),(31,'Can delete node setting',8,'delete_nodesetting'),(32,'Can view node setting',8,'view_nodesetting'),(33,'Can add node',9,'add_node'),(34,'Can change node',9,'change_node'),(35,'Can delete node',9,'delete_node'),(36,'Can view node',9,'view_node'),(37,'Can add node required setting',10,'add_noderequiredsetting'),(38,'Can change node required setting',10,'change_noderequiredsetting'),(39,'Can delete node required setting',10,'delete_noderequiredsetting'),(40,'Can view node required setting',10,'view_noderequiredsetting');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$180000$WoHrfUHTCUXU$fEp2BBx0veZ2eKFMZH9Hiqing+LHUFXJezE33CMU2ss=','2019-12-19 09:22:52.971325',1,'admin','','','',1,1,'2019-12-06 05:03:26.685026');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-12-06 05:15:45.174430','1','NodeType object (1)',1,'[{\"added\": {}}]',7,1),(2,'2019-12-06 09:08:35.065099','1','NodeType object (1)',2,'[{\"changed\": {\"fields\": [\"Required settings\"]}}]',7,1),(3,'2019-12-06 09:08:54.226875','2','NodeType object (2)',1,'[{\"added\": {}}]',7,1),(4,'2019-12-06 09:09:20.877814','3','NodeType object (3)',1,'[{\"added\": {}}]',7,1),(5,'2019-12-06 09:09:53.213219','4','NodeType object (4)',1,'[{\"added\": {}}]',7,1),(6,'2019-12-06 09:29:09.889642','4','TTS Play Menu',2,'[{\"changed\": {\"fields\": [\"Required settings\"]}}]',7,1),(7,'2019-12-06 09:29:21.715564','3','Play Menu',2,'[{\"changed\": {\"fields\": [\"Required settings\"]}}]',7,1),(8,'2019-12-06 09:29:28.006122','2','TTS Play',2,'[{\"changed\": {\"fields\": [\"Required settings\"]}}]',7,1),(9,'2019-12-06 09:29:34.873283','1','Play',2,'[{\"changed\": {\"fields\": [\"Required settings\"]}}]',7,1),(10,'2019-12-18 15:31:28.071644','1','NodeRequiredSetting object (1)',1,'[{\"added\": {}}]',10,1),(11,'2019-12-18 15:31:32.670608','1','NodeRequiredSetting object (1)',2,'[]',10,1),(12,'2019-12-18 15:32:04.176554','2','NodeRequiredSetting object (2)',1,'[{\"added\": {}}]',10,1),(13,'2019-12-18 15:32:37.377333','3','NodeRequiredSetting object (3)',1,'[{\"added\": {}}]',10,1),(14,'2019-12-18 15:33:06.971571','4','NodeRequiredSetting object (4)',1,'[{\"added\": {}}]',10,1),(15,'2019-12-18 15:34:06.170891','5','NodeRequiredSetting object (5)',1,'[{\"added\": {}}]',10,1),(16,'2019-12-18 15:34:25.845462','6','NodeRequiredSetting object (6)',1,'[{\"added\": {}}]',10,1),(17,'2019-12-18 15:34:53.732595','7','NodeRequiredSetting object (7)',1,'[{\"added\": {}}]',10,1),(18,'2019-12-18 16:00:27.753679','1','Play',2,'[{\"changed\": {\"fields\": [\"Required settings id\", \"Atrributes\", \"Created at\", \"Updated at\"]}}]',7,1),(19,'2019-12-18 16:01:41.229244','2','TTS Play',2,'[{\"changed\": {\"fields\": [\"Required settings id\", \"Atrributes\", \"Created at\", \"Updated at\"]}}]',7,1),(20,'2019-12-18 16:02:47.616024','4','TTS Play Menu',3,'',7,1),(21,'2019-12-18 16:02:47.653983','3','Play Menu',3,'',7,1),(22,'2019-12-18 16:02:47.688383','2','TTS Play',3,'',7,1),(23,'2019-12-18 16:02:47.722416','1','Play',3,'',7,1),(24,'2019-12-18 16:03:33.583498','5','NodeRequiredSetting object (5)',2,'[{\"changed\": {\"fields\": [\"Name\"]}}]',10,1),(25,'2019-12-18 16:03:50.405591','4','NodeRequiredSetting object (4)',2,'[{\"changed\": {\"fields\": [\"Name\"]}}]',10,1),(26,'2019-12-18 16:04:01.925959','3','NodeRequiredSetting object (3)',2,'[{\"changed\": {\"fields\": [\"Name\"]}}]',10,1),(27,'2019-12-18 16:04:15.807716','2','NodeRequiredSetting object (2)',2,'[{\"changed\": {\"fields\": [\"Name\"]}}]',10,1),(28,'2019-12-18 16:04:29.598748','1','NodeRequiredSetting object (1)',2,'[{\"changed\": {\"fields\": [\"Name\"]}}]',10,1),(29,'2019-12-18 16:04:34.828774','5','NodeRequiredSetting object (5)',2,'[]',10,1),(30,'2019-12-18 16:05:09.772747','5','play',1,'[{\"added\": {}}]',7,1),(31,'2019-12-18 16:05:32.499016','6','play_tts',1,'[{\"added\": {}}]',7,1),(32,'2019-12-18 16:06:00.607825','7','play_menu',1,'[{\"added\": {}}]',7,1),(33,'2019-12-18 16:06:23.678015','8','play_menu_tts',1,'[{\"added\": {}}]',7,1),(34,'2019-12-18 16:07:01.031597','9','play_number',1,'[{\"added\": {}}]',7,1),(35,'2019-12-18 16:07:22.323502','10','dial_number',1,'[{\"added\": {}}]',7,1),(36,'2019-12-19 13:13:35.456260','ffb531e-bfcf-d67-1e5d-4bc88a8c14a','Node object (ffb531e-bfcf-d67-1e5d-4bc88a8c14a)',3,'',9,1),(37,'2019-12-19 13:13:36.780685','fc85bcc-7e06-2b3e-6c5a-41220c5ef366','Node object (fc85bcc-7e06-2b3e-6c5a-41220c5ef366)',3,'',9,1),(38,'2019-12-19 13:13:36.837219','e4d5a-b214-de23-e4f3-41abf8d7e54c','Node object (e4d5a-b214-de23-e4f3-41abf8d7e54c)',3,'',9,1),(39,'2019-12-19 13:13:36.903220','d577b3f-55d4-7c5-81c3-8865f01ceae','Node object (d577b3f-55d4-7c5-81c3-8865f01ceae)',3,'',9,1),(40,'2019-12-19 13:13:36.959211','d1421fe-2ab7-f1ed-af22-cbbfc6c4ae','Node object (d1421fe-2ab7-f1ed-af22-cbbfc6c4ae)',3,'',9,1),(41,'2019-12-19 13:13:37.113896','cac6f00-d33-150b-b502-ff6e0ec87dc','Node object (cac6f00-d33-150b-b502-ff6e0ec87dc)',3,'',9,1),(42,'2019-12-19 13:13:37.282156','8e63f20-510-e3c8-1d3-e7740f8aa51b','Node object (8e63f20-510-e3c8-1d3-e7740f8aa51b)',3,'',9,1),(43,'2019-12-19 13:13:37.467610','8b2b8e-f3f3-d4c2-c4f2-48a41a020bb','Node object (8b2b8e-f3f3-d4c2-c4f2-48a41a020bb)',3,'',9,1),(44,'2019-12-19 13:13:37.950812','6e15542-da-558a-1501-a5cc7767dd7','Node object (6e15542-da-558a-1501-a5cc7767dd7)',3,'',9,1),(45,'2019-12-19 13:13:38.171692','67a353f-753e-f3e4-0e61-36aa743472','Node object (67a353f-753e-f3e4-0e61-36aa743472)',3,'',9,1),(46,'2019-12-19 13:13:38.261673','6612dcc-6d35-d12b-466b-2653c240dc1','Node object (6612dcc-6d35-d12b-466b-2653c240dc1)',3,'',9,1),(47,'2019-12-19 13:13:38.405675','5d32518-1264-bd27-64e-05cf1c82002a','Node object (5d32518-1264-bd27-64e-05cf1c82002a)',3,'',9,1),(48,'2019-12-19 13:13:38.572926','52752af-872-7560-f52c-3edb6c63a5','Node object (52752af-872-7560-f52c-3edb6c63a5)',3,'',9,1),(49,'2019-12-19 13:13:38.695687','515dee-1207-eaa-3db6-eb83cf4c0aaf','Node object (515dee-1207-eaa-3db6-eb83cf4c0aaf)',3,'',9,1),(50,'2019-12-19 13:13:38.819235','440c84-518b-bee1-a3b3-5d1df3ea2bb5','Node object (440c84-518b-bee1-a3b3-5d1df3ea2bb5)',3,'',9,1),(51,'2019-12-19 13:13:38.995566','32dae26-f55c-a2f0-1ab2-38afa517c81','Node object (32dae26-f55c-a2f0-1ab2-38afa517c81)',3,'',9,1),(52,'2019-12-19 13:13:39.263447','2033bff-cc-d3d2-b6b6-b505e1335c76','Node object (2033bff-cc-d3d2-b6b6-b505e1335c76)',3,'',9,1),(53,'2019-12-19 13:13:39.319298','0dc4b1d-eb-4263-ce1-36b750772a5b','Node object (0dc4b1d-eb-4263-ce1-36b750772a5b)',3,'',9,1),(54,'2019-12-19 13:14:04.150450','10','NodeType object (10)',3,'',7,1),(55,'2019-12-19 13:14:04.203224','9','NodeType object (9)',3,'',7,1),(56,'2019-12-19 13:14:04.261237','8','NodeType object (8)',3,'',7,1),(57,'2019-12-19 13:14:04.315040','7','NodeType object (7)',3,'',7,1),(58,'2019-12-19 13:14:04.348833','6','NodeType object (6)',3,'',7,1),(59,'2019-12-19 13:14:04.405966','5','NodeType object (5)',3,'',7,1),(60,'2019-12-19 13:15:53.832541','6','NodeSetting object (6)',3,'',8,1),(61,'2019-12-19 13:15:53.892172','5','NodeSetting object (5)',3,'',8,1),(62,'2019-12-19 13:15:53.978386','4','NodeSetting object (4)',3,'',8,1),(63,'2019-12-19 13:15:54.080300','3','NodeSetting object (3)',3,'',8,1),(64,'2019-12-19 13:15:54.136495','2','NodeSetting object (2)',3,'',8,1),(65,'2019-12-19 13:15:54.194313','1','NodeSetting object (1)',3,'',8,1),(66,'2019-12-19 13:17:52.510723','11','NodeType object (11)',1,'[{\"added\": {}}]',7,1),(67,'2019-12-19 13:18:17.745206','12','NodeType object (12)',1,'[{\"added\": {}}]',7,1),(68,'2019-12-19 13:19:01.104496','13','NodeType object (13)',1,'[{\"added\": {}}]',7,1),(69,'2019-12-19 13:19:57.240964','14','NodeType object (14)',1,'[{\"added\": {}}]',7,1),(70,'2019-12-19 13:20:17.200951','15','NodeType object (15)',1,'[{\"added\": {}}]',7,1),(71,'2019-12-19 13:20:43.032185','16','NodeType object (16)',1,'[{\"added\": {}}]',7,1),(72,'2019-12-20 05:30:27.647075','fd4186e-088b-1f1d-7c07-c5ec056bf54','Node object (fd4186e-088b-1f1d-7c07-c5ec056bf54)',3,'',9,1),(73,'2019-12-20 05:30:27.821550','fd2b67-c64f-5f3a-5c53-7cede32cb43','Node object (fd2b67-c64f-5f3a-5c53-7cede32cb43)',3,'',9,1),(74,'2019-12-20 05:30:27.855268','2f64faf-a1a8-1055-588c-016de7ca73f0','Node object (2f64faf-a1a8-1055-588c-016de7ca73f0)',3,'',9,1),(75,'2019-12-20 05:30:46.637877','9','NodeSetting object (9)',3,'',8,1),(76,'2019-12-20 05:30:46.683180','8','NodeSetting object (8)',3,'',8,1),(77,'2019-12-20 05:30:46.715256','7','NodeSetting object (7)',3,'',8,1),(78,'2019-12-20 05:54:42.633092','a1fed66-b887-387b-2e06-71b14a3844a','Node object (a1fed66-b887-387b-2e06-71b14a3844a)',3,'',9,1),(79,'2019-12-20 05:54:43.047462','a1dbdda-2821-7258-e063-faae41657e6','Node object (a1dbdda-2821-7258-e063-faae41657e6)',3,'',9,1),(80,'2019-12-20 05:54:43.081310','7378aea-e302-ed15-dfde-1ef78be5bfc','Node object (7378aea-e302-ed15-dfde-1ef78be5bfc)',3,'',9,1),(81,'2019-12-20 05:54:43.115260','4c26af7-052b-faae-48ae-22ae82e5f7','Node object (4c26af7-052b-faae-48ae-22ae82e5f7)',3,'',9,1),(82,'2019-12-20 05:54:43.149419','21fee40-4f7-a2c-4aa-138574c748','Node object (21fee40-4f7-a2c-4aa-138574c748)',3,'',9,1),(83,'2019-12-20 05:54:43.183095','1e41d7c-ef7e-da62-d7c2-d78b0d6c0842','Node object (1e41d7c-ef7e-da62-d7c2-d78b0d6c0842)',3,'',9,1),(84,'2019-12-20 05:54:43.216779','143b255-754-257d-bc26-da30d5ca850','Node object (143b255-754-257d-bc26-da30d5ca850)',3,'',9,1),(85,'2019-12-20 05:54:43.250501','0815fe7-c7b8-5e3-15a-7010328c0dd','Node object (0815fe7-c7b8-5e3-15a-7010328c0dd)',3,'',9,1),(86,'2019-12-20 05:55:01.925171','20','NodeSetting object (20)',3,'',8,1),(87,'2019-12-20 05:55:02.097534','19','NodeSetting object (19)',3,'',8,1),(88,'2019-12-20 05:55:02.200108','18','NodeSetting object (18)',3,'',8,1),(89,'2019-12-20 05:55:02.234095','17','NodeSetting object (17)',3,'',8,1),(90,'2019-12-20 05:55:02.269098','16','NodeSetting object (16)',3,'',8,1),(91,'2019-12-20 05:55:02.301552','15','NodeSetting object (15)',3,'',8,1),(92,'2019-12-20 05:55:02.335413','14','NodeSetting object (14)',3,'',8,1),(93,'2019-12-20 05:55:02.369388','13','NodeSetting object (13)',3,'',8,1),(94,'2019-12-20 05:55:02.402679','12','NodeSetting object (12)',3,'',8,1),(95,'2019-12-20 05:55:02.436777','11','NodeSetting object (11)',3,'',8,1),(96,'2019-12-20 05:55:02.471221','10','NodeSetting object (10)',3,'',8,1),(97,'2019-12-20 06:17:14.700234','ee63ef-7652-2db-2a3f-4c25aaee06','Node object (ee63ef-7652-2db-2a3f-4c25aaee06)',3,'',9,1),(98,'2019-12-20 06:17:15.821112','7fb44b-bab6-e0bd-e8e7-fb128faabd8','Node object (7fb44b-bab6-e0bd-e8e7-fb128faabd8)',3,'',9,1),(99,'2019-12-20 06:17:15.933875','6f0bbef-8070-b7a1-0b53-62ac6f08447','Node object (6f0bbef-8070-b7a1-0b53-62ac6f08447)',3,'',9,1),(100,'2019-12-20 06:17:15.967978','5b407bc-7e8-542b-027c-3bf18ea8e45','Node object (5b407bc-7e8-542b-027c-3bf18ea8e45)',3,'',9,1),(101,'2019-12-20 06:17:16.001938','55cdf-8eee-8b7b-8164-adba4c47c60','Node object (55cdf-8eee-8b7b-8164-adba4c47c60)',3,'',9,1),(102,'2019-12-20 06:17:31.993721','30','NodeSetting object (30)',3,'',8,1),(103,'2019-12-20 06:17:33.114292','29','NodeSetting object (29)',3,'',8,1),(104,'2019-12-20 06:17:33.181758','28','NodeSetting object (28)',3,'',8,1),(105,'2019-12-20 06:17:33.215492','27','NodeSetting object (27)',3,'',8,1),(106,'2019-12-20 06:17:33.249843','26','NodeSetting object (26)',3,'',8,1),(107,'2019-12-20 06:17:33.294781','25','NodeSetting object (25)',3,'',8,1),(108,'2019-12-20 06:17:33.328847','24','NodeSetting object (24)',3,'',8,1),(109,'2019-12-20 06:17:33.362549','23','NodeSetting object (23)',3,'',8,1),(110,'2019-12-20 06:17:33.407122','22','NodeSetting object (22)',3,'',8,1),(111,'2019-12-20 06:17:33.441147','21','NodeSetting object (21)',3,'',8,1),(112,'2019-12-20 06:58:40.680415','edd081d-156-bdcc-fe8c-acaf2350b30f','Node object (edd081d-156-bdcc-fe8c-acaf2350b30f)',3,'',9,1),(113,'2019-12-20 06:58:42.229288','b61b2-4545-a730-d5d2-6b6ddef6eb1','Node object (b61b2-4545-a730-d5d2-6b6ddef6eb1)',3,'',9,1),(114,'2019-12-20 06:58:42.612765','b04e8b7-3c4f-ab3-76c4-6dc07650e6cc','Node object (b04e8b7-3c4f-ab3-76c4-6dc07650e6cc)',3,'',9,1),(115,'2019-12-20 06:58:42.843197','a217836-0b63-760f-e18d-c4ea41e823','Node object (a217836-0b63-760f-e18d-c4ea41e823)',3,'',9,1),(116,'2019-12-20 06:58:43.198288','850dfc-4188-137d-02f-1d2516bae1cb','Node object (850dfc-4188-137d-02f-1d2516bae1cb)',3,'',9,1),(117,'2019-12-20 06:58:43.232483','556281d-38fe-26b8-7408-b47d004745b7','Node object (556281d-38fe-26b8-7408-b47d004745b7)',3,'',9,1),(118,'2019-12-20 06:58:43.266545','205accd-c7c4-2e55-16e5-6ee0c137e50','Node object (205accd-c7c4-2e55-16e5-6ee0c137e50)',3,'',9,1),(119,'2019-12-20 06:58:58.424683','41','NodeSetting object (41)',3,'',8,1),(120,'2019-12-20 06:58:58.459063','40','NodeSetting object (40)',3,'',8,1),(121,'2019-12-20 06:58:58.493096','39','NodeSetting object (39)',3,'',8,1),(122,'2019-12-20 06:58:58.526730','38','NodeSetting object (38)',3,'',8,1),(123,'2019-12-20 06:58:58.560529','37','NodeSetting object (37)',3,'',8,1),(124,'2019-12-20 06:58:58.594676','36','NodeSetting object (36)',3,'',8,1),(125,'2019-12-20 06:58:58.629207','35','NodeSetting object (35)',3,'',8,1),(126,'2019-12-20 06:58:58.663158','34','NodeSetting object (34)',3,'',8,1),(127,'2019-12-20 06:58:58.697009','33','NodeSetting object (33)',3,'',8,1),(128,'2019-12-20 06:58:58.730878','32','NodeSetting object (32)',3,'',8,1),(129,'2019-12-20 06:58:58.764207','31','NodeSetting object (31)',3,'',8,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(9,'ivrnode','node'),(10,'ivrnode','noderequiredsetting'),(8,'ivrnode','nodesetting'),(7,'ivrnode','nodetype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-12-06 05:01:10.940428'),(2,'auth','0001_initial','2019-12-06 05:01:14.260974'),(3,'admin','0001_initial','2019-12-06 05:01:29.992609'),(4,'admin','0002_logentry_remove_auto_add','2019-12-06 05:01:33.420764'),(5,'admin','0003_logentry_add_action_flag_choices','2019-12-06 05:01:33.544288'),(6,'contenttypes','0002_remove_content_type_name','2019-12-06 05:01:36.248471'),(7,'auth','0002_alter_permission_name_max_length','2019-12-06 05:01:36.535060'),(8,'auth','0003_alter_user_email_max_length','2019-12-06 05:01:36.779947'),(9,'auth','0004_alter_user_username_opts','2019-12-06 05:01:36.878124'),(10,'auth','0005_alter_user_last_login_null','2019-12-06 05:01:38.228876'),(11,'auth','0006_require_contenttypes_0002','2019-12-06 05:01:38.326217'),(12,'auth','0007_alter_validators_add_error_messages','2019-12-06 05:01:38.411020'),(13,'auth','0008_alter_user_username_max_length','2019-12-06 05:01:38.750734'),(14,'auth','0009_alter_user_last_name_max_length','2019-12-06 05:01:39.079935'),(15,'auth','0010_alter_group_name_max_length','2019-12-06 05:01:39.342954'),(16,'auth','0011_update_proxy_permissions','2019-12-06 05:01:39.442271'),(17,'auth','0012_auto_20190512_0027','2019-12-06 05:01:41.885566'),(18,'auth','0013_auto_20190624_0851','2019-12-06 05:01:42.111531'),(19,'ivrnode','0001_initial','2019-12-06 05:01:47.268677'),(20,'ivrnode','0002_auto_20191206_0440','2019-12-06 05:01:52.652053'),(21,'ivrnode','0003_auto_20191206_0444','2019-12-06 05:01:55.270396'),(22,'ivrnode','0004_auto_20191206_0445','2019-12-06 05:01:57.784668'),(23,'sessions','0001_initial','2019-12-06 05:01:59.197187'),(24,'ivrnode','0005_auto_20191206_0954','2019-12-06 09:54:28.691750'),(25,'ivrnode','0006_auto_20191209_0526','2019-12-09 05:26:12.166111'),(26,'ivrnode','0007_auto_20191219_1122','2019-12-19 11:22:20.586607'),(27,'ivrnode','0008_auto_20191219_1131','2019-12-19 11:32:05.127549');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('09r4g0tuf0v5yndo7xaga8r33pd44580','Mzg4NmY1YjEzODgxMGQxZmM4YjRjZWFjNjVkYjhhMGQ4NjgwNTY4ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxMTZiYzgwZTk4OTAwOTNlZDdmODhiM2Q5ZmRmZDFmNTRhNGFiZjFjIn0=','2020-01-01 15:24:06.481036'),('cjelecarei3zsc4xopsz1wzmskt88znf','Mzg4NmY1YjEzODgxMGQxZmM4YjRjZWFjNjVkYjhhMGQ4NjgwNTY4ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxMTZiYzgwZTk4OTAwOTNlZDdmODhiM2Q5ZmRmZDFmNTRhNGFiZjFjIn0=','2019-12-20 05:10:05.439632'),('h1dax3lbay46692bghicodgx710gxbjs','Mzg4NmY1YjEzODgxMGQxZmM4YjRjZWFjNjVkYjhhMGQ4NjgwNTY4ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxMTZiYzgwZTk4OTAwOTNlZDdmODhiM2Q5ZmRmZDFmNTRhNGFiZjFjIn0=','2020-01-02 09:22:53.018407');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ivrnode_node`
--

DROP TABLE IF EXISTS `ivrnode_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ivrnode_node` (
  `id` varchar(100) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `parent` varchar(200) DEFAULT NULL,
  `response` varchar(100) DEFAULT NULL,
  `key` varchar(150) DEFAULT NULL,
  `ivr_id` varchar(500) DEFAULT NULL,
  `node_type_id` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ivrnode_node`
--

LOCK TABLES `ivrnode_node` WRITE;
/*!40000 ALTER TABLE `ivrnode_node` DISABLE KEYS */;
INSERT INTO `ivrnode_node` VALUES ('','{\'name\': \'\', \'ivr_id\': 1, \'node_type_id\': \'5\', \'parent\': \'\', \'response\': \'\', \'key\': \'\', \'id\': 65}',NULL,NULL,NULL,NULL,NULL),('205accd-c7c4-2e55-16e5-6ee0c137e50','TTS NODE','556281d-38fe-26b8-7408-b47d004745b7','','','1','12'),('43b1c03-d03-2acf-ef2e-38500f045a0b','MENU 2 CHILD NODE TO CONFIRM','b04e8b7-3c4f-ab3-76c4-6dc07650e6cc','','1','1','11'),('556281d-38fe-26b8-7408-b47d004745b7','HELLO','','222','','1','11'),('850dfc-4188-137d-02f-1d2516bae1cb','MENU 1 CHILD NODE SALES','b61b2-4545-a730-d5d2-6b6ddef6eb1','','1','1','11'),('a217836-0b63-760f-e18d-c4ea41e823','COMPANY NAME NODE','edd081d-156-bdcc-fe8c-acaf2350b30f','','','1','11'),('b04e8b7-3c4f-ab3-76c4-6dc07650e6cc','MENU 1 CHILD NODE TO CONFIRM ORDER','b61b2-4545-a730-d5d2-6b6ddef6eb1','','2','1','13'),('b61b2-4545-a730-d5d2-6b6ddef6eb1','MENU 1','a217836-0b63-760f-e18d-c4ea41e823','444','','1','13'),('edd081d-156-bdcc-fe8c-acaf2350b30f','ORDER STATEMENT NODE','205accd-c7c4-2e55-16e5-6ee0c137e50','','','1','11'),('f6a1ea-1315-c682-f83f-36a7b400b46','MENU 2 CHILD NODE TO CANCEL','b04e8b7-3c4f-ab3-76c4-6dc07650e6cc','','2','1','11');
/*!40000 ALTER TABLE `ivrnode_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ivrnode_noderequiredsetting`
--

DROP TABLE IF EXISTS `ivrnode_noderequiredsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ivrnode_noderequiredsetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `default_value` varchar(30) NOT NULL,
  `field_type` varchar(40) NOT NULL,
  `created_at` varchar(50) NOT NULL,
  `updated_at` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ivrnode_noderequiredsetting`
--

LOCK TABLES `ivrnode_noderequiredsetting` WRITE;
/*!40000 ALTER TABLE `ivrnode_noderequiredsetting` DISABLE KEYS */;
INSERT INTO `ivrnode_noderequiredsetting` VALUES (1,'repeat_menu_count','3','text','test','test'),(2,'repeat_menu_key','9','text','test','test'),(3,'dial_number','test','text','test','test'),(4,'webhook_url','test','text','test','test'),(5,'variable_name','test','text','test','test'),(6,'filename','test','text','test','test'),(7,'nextnode','test','text','test','test');
/*!40000 ALTER TABLE `ivrnode_noderequiredsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ivrnode_nodesetting`
--

DROP TABLE IF EXISTS `ivrnode_nodesetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ivrnode_nodesetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `required_settings_id` varchar(30) DEFAULT NULL,
  `settings_value` varchar(100) DEFAULT NULL,
  `node_id` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ivrnode_nodesetting`
--

LOCK TABLES `ivrnode_nodesetting` WRITE;
/*!40000 ALTER TABLE `ivrnode_nodesetting` DISABLE KEYS */;
INSERT INTO `ivrnode_nodesetting` VALUES (42,'6','HELLO','556281d-38fe-26b8-7408-b47d004745b7'),(43,'5','ANUJ VAR NAME','205accd-c7c4-2e55-16e5-6ee0c137e50'),(44,'6','YOUR ORDER HAS BEEN PLACED BY','edd081d-156-bdcc-fe8c-acaf2350b30f'),(45,'6','SNAPDEAL','a217836-0b63-760f-e18d-c4ea41e823'),(46,'1','3','b61b2-4545-a730-d5d2-6b6ddef6eb1'),(47,'2','9','b61b2-4545-a730-d5d2-6b6ddef6eb1'),(48,'6','PRESS  1 FOR SALES, PRESS 2 TO CONFIRM YOUR ORDER','b61b2-4545-a730-d5d2-6b6ddef6eb1'),(49,'1','3','b04e8b7-3c4f-ab3-76c4-6dc07650e6cc'),(50,'2','9','b04e8b7-3c4f-ab3-76c4-6dc07650e6cc'),(51,'6','PRESS 1 TO CONFIRM ORDER PRESS 2 TO CANCEL','b04e8b7-3c4f-ab3-76c4-6dc07650e6cc'),(52,'6','THANK YOU, YOUR ORDER HAS BEEN CANCELED','f6a1ea-1315-c682-f83f-36a7b400b46'),(53,'6','THANK YOU, YOUR ORDER HAS BEEN CONFIRMD','43b1c03-d03-2acf-ef2e-38500f045a0b'),(54,'6','THANK YOU','850dfc-4188-137d-02f-1d2516bae1cb');
/*!40000 ALTER TABLE `ivrnode_nodesetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ivrnode_nodetype`
--

DROP TABLE IF EXISTS `ivrnode_nodetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ivrnode_nodetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `atrributes` varchar(30) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `required_settings_id` varchar(60) DEFAULT NULL,
  `updated_at` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ivrnode_nodetype`
--

LOCK TABLES `ivrnode_nodetype` WRITE;
/*!40000 ALTER TABLE `ivrnode_nodetype` DISABLE KEYS */;
INSERT INTO `ivrnode_nodetype` VALUES (11,'play','file','test','6','tst'),(12,'play _tts','text','t','5','t'),(13,'play_menu','text','tt','1,2,6','tt'),(14,'play_menu_tts','text','t','5','t'),(15,'play_number','text','t','5','t'),(16,'dial_number','text','t','3','t');
/*!40000 ALTER TABLE `ivrnode_nodetype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-20 17:56:57

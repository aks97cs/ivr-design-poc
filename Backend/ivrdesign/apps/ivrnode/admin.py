from django.contrib import admin
from .models import Node
from .models import NodeType
from .models import NodeSetting
from .models import NodeRequiredSetting
# Register your models here.

class NodeAdmin(admin.ModelAdmin):
	pass

class NodeTypeAdmin(admin.ModelAdmin):
	pass

class NodeSettingAdmin(admin.ModelAdmin):
	pass

class NodeRequiredSettingAdmin(admin.ModelAdmin):
	pass

admin.site.register(Node, NodeAdmin)
admin.site.register(NodeType, NodeTypeAdmin)
admin.site.register(NodeSetting, NodeSettingAdmin)
admin.site.register(NodeRequiredSetting, NodeRequiredSettingAdmin)

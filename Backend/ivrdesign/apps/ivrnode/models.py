from django.db import models

# Create your models here.
class NodeType(models.Model):
	name = models.CharField(max_length=30, null = True)
	required_settings_id = models.CharField(max_length=60, null= True)
	atrributes = models.CharField(max_length=30, null=True)
	created_at = models.CharField(max_length=30, null=True)
	updated_at = models.CharField(max_length=40, null=True)
	# def __str__(self):
	# 	return self.name

class Node(models.Model):
	name = models.CharField(max_length=1000)
	ivr_id = models.CharField(max_length=500, null=True)
	node_type_id =  models.CharField(max_length = 200, null = True)
	parent = models.CharField(max_length = 200, null = True)
	response = models.CharField(max_length=100, null = True)
	key = models.CharField(max_length=150, null = True)
	id = models.CharField(max_length=100, primary_key= True)
	# def __str__(self):
	# 	return self.name
	# ivr_id = models.CharField(max_length = 100)

class NodeSetting(models.Model):
	node_id =  models.CharField(max_length = 200, null = True)
	required_settings_id = models.CharField(max_length=30, null = True)
	settings_value = models.CharField(max_length=100, null=True)


class NodeRequiredSetting(models.Model):
	name = models.CharField(max_length=40)
	default_value = models.CharField(max_length=30)
	field_type = models.CharField(max_length=40)
	created_at = models.CharField(max_length=50)
	updated_at = models.CharField(max_length=50) 



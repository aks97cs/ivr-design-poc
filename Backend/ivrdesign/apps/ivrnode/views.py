from django.shortcuts import render
from .models import NodeType
import json
from django.http import JsonResponse
from .models import NodeType, NodeRequiredSetting, Node, NodeSetting
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.
@csrf_exempt
def get_noode_type(request):
	n_type = NodeType.objects.all()
	print(n_type)
	data = []
	n = {}
	for node in n_type:
		n['name'] = node.name
		n['key'] = node.id
		n['id']=node.id
		data.append(n)
		n={}
	return JsonResponse({"status":'success', 'data': data})


@csrf_exempt
def get_required_field(request):

	req_setng = NodeType.objects.get(id=request.POST['id'])
	id_data = str(req_setng.required_settings_id).split(',')
	id_data = tuple(id_data)
	data = NodeRequiredSetting.objects.filter(id__in = id_data)
	r_data = []
	n = {}
	if(len(data)>1):
		for r_f in range(0, len(data)):
			print("testtttt")
			n['name']=data[r_f].name
			n['type'] = data[r_f].field_type
			n['value'] = data[r_f].default_value
			print(n)
			r_data.append(n)
			n = {}
		return JsonResponse({'status':'success', 'data':r_data})
	else:
		return JsonResponse({"status":'success', 'data':[{'name':data[0].name, 'type':data[0].field_type, 'value':data[0].default_value}]})


def seting_helper(node_id):
	req_setng = NodeType.objects.get(id =node_id)
	id_data = str(req_setng.required_settings_id).split(',')
	id_data = tuple(id_data)
	data = NodeRequiredSetting.objects.filter(id__in = id_data)
	r_data = []
	n = {}
	# if(len(data)>1):
	for r_f in range(0, len(data)):
		# print("testtttt")
		n['name']=data[r_f].name
		n['type'] = data[r_f].field_type
		n['value'] = data[r_f].default_value
		n['id'] = data[r_f].id
		print(n)
		r_data.append(n)
		n = {}
	return r_data

def prepare_node_data(node):
	data = {
		'name': node['name'],
		'ivr_id': 1,
		'node_type_id': node['node_type'],
		'parent': node['parent_id'],
		'response': node['response'],
		'key': node['key'],
		'id': node['id']
	}
	return data

def prepare_node_setting_data(node):
	helper = seting_helper(node['node_type'])
	data = {}
	print("my menu helper")
	print(helper)
	for settings in helper:
		print(settings)
		data['required_settings_id'] = settings['id']
		data['settings_value'] = node[settings['name']]
		data['node_id'] = node['id']
		sett = NodeSetting(**data)
		sett.save()

	data = {}

	



@csrf_exempt
def save_node_data(request):
	print("node datat")
	print(request.POST)
	data = json.loads(request.POST['data'])
	print(data)
	for node in data:
		node_data = prepare_node_data(node)
		nn = Node(**node_data)
		nn.save()
		node_setting = prepare_node_setting_data(node)

	return JsonResponse({'status':'success'})

def edit_node_data(request):
	pass

def delete_node_data(request):
	pass

from django.apps import AppConfig


class IvrnodeConfig(AppConfig):
    name = 'ivrnode'

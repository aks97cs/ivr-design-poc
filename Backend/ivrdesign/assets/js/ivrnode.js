let ivr_node = [];
var counterId= 0;
const fetchNodeProps = (nodeId) => {
	for(let i=0;i<ivr_node.length;i++){
		if(ivr_node[i].id==nodeId){
			return ivr_node[i];
		}
		for(let j=0;j<ivr_node[i].childNode.length;j++){
			if(ivr_node[i].childNode[j].id==nodeId){
				return ivr_node[i].childNode[j];
			}
		}
	}
}

// handle child bra

const branchNodeButton = (nodeId) => `<td rowspan="2">
							<button class = "btn btn-primary" onclick="nodeDetail(${nodeId}, ${true})" data-toggle="modal" data-target="#exampleModalLong" >Add Branch Node</button>
						</td>`;


const addNodeParent = (nodeId) => `<td rowspan="2">
								<span>Parent id = ${nodeId}</span>
						</td>`;

const deleteButton = (nodeId) => `
	<td rowspan="2">
		<button class = "btn btn-primary" onclick="removeNode(${nodeId})">Delete</button>
	</td>
`;

const childNodeField = (props, isChildNode) => {

	console.log("in my childNodeField");
	console.log(props);
	console.log(isChildNode);


	if(isChildNode){
		return `<input type="text" name="parent" value="${props.id}"/>`;
	}

	return '';
}

const saveButtonBehav = (props) => {
	const buttonhtml = Object.keys(props).length == 0 ?`save-node"`:`" onclick=save_node_data(${props.id})`;
	return buttonhtml;
}

let nodeDetail = (props = {}, isChildNode=false) => {
	const instanceType = (props instanceof Object);

	console.log(instanceType);
	if(!instanceType){
		props = fetchNodeProps(props);
	}
	const parentId = isChildNode ? props.id : '';
	console.log("my parentId ===", parentId) ;
	console.log(parentId);

	const html = `

				<div class="card" style="width: 18rem;" id = "node-attr">
					<h3>Node attribute</h3>
				  <div class="card-body">
					  <form action="" id="abc" name="myform">
						<input type="text" name="name" placeholder="Please enter the node name" value="${props.name != null?props.name:''}"/>
						<span>Node type</span><br>
						<select name="type_id">
							<option value="">Select Node type</option>
							<option value = "1">Play</option>
							<option value='2'>Play TTS</option>
							<option value='3'>Play Menu</option>
							<option value='4'>Play TTS Menu</option>
						</select>
						<input type="hidden" name="key" value="${props.key!=null?props.key:''}" />
						<pre> adding new valkues ==== ${parentId}</pre>
						<input type="text" name="parent" value="${parentId}" />
						<hr>
						<h3>Required field</h3>
						<input type="text" name="field1" placeholder="Required field1" value="${props.field1!=null?props.field1:''}" />
						<input type="text" name="field2" placeholder="Required field1" value="${props.field2!=null?props.field2:''}"/>
						<hr>
						<textarea  name="response" placeholder="Response" value="${props.response!=null?props.response:''}"></textarea>
					    <input type="submit" class="btn btn-primary ${saveButtonBehav(props)} data-dismiss="modal"  value="Save" >

					  </form>
				  </div>
				</div>

`;
return html;
}


function compact_node(props){
	let html = "";
	$.each(props, function(index, value){
		html += `

				<div class="card ivr-node">
					<div class="card-body">
						<table border="2">
							<tr>
								<td>NodeID</td>
								<td >NodeName</td>
								<td>NodeType</td>
								<td rowspan="2"><button class="btn btn-primary" onclick=editNodeDetails(${value.id}) data-toggle="modal" data-target="#exampleModalLong">Edit</button></td>
								${config.is_menu.indexOf(value.type_id) !== -1 && branchNodeButton(value.id)}
								${value.parent != null && addNodeParent(value.parent) }
							</tr>
						<tr>
								<td>${value.id}</td>
								<td>${value.name}</td>
								<td>${value.type_id}</td>
						</tr>
						</table>
					</div>
				</div>
		`;
		if(value.childNode.length > 0) {
			$.each(value.childNode, function(c_index, c_value){
				html += `

					<div class="card ivr-node">
						<div class="card-body">
							<table border="2">
								<tr>
									<td>NodeID</td>
									<td >NodeName</td>
									<td>NodeType</td>
									<td rowspan="2"><button class="btn btn-primary" onclick=nodeDetail(${c_value.id}) data-toggle="modal" data-target="#exampleModalLong">Edit</button></td>
									${config.is_menu.indexOf(c_value.type_id) !== -1 && branchNodeButton(c_value.id)}
									${c_value.parent != null && addNodeParent(c_value.parent) }
								</tr>
							<tr>
									<td>${c_value.id}</td>
									<td>${c_value.name}</td>
									<td>${c_value.type_id}</td>
							</tr>
							</table>
						</div>
					</div>
				`;
			});
		}


	});
	return html
}


function editNodeDetails(nodeId){
	let fetchNode = fetchNodeProps(nodeId);
	console.log("my node props");
	console.log(fetchNode);
	html = nodeDetail(fetchNode);
	$(".ivr-node-container").html(html);
}

function addBranchNodeDetails(nodeId){
	let fetchNode = fetchNodeProps(nodeId);
	let html = `

				<div class="card" style="width: 18rem;" id = "node-attr">
					<h3>Node attribute</h3>
				  <div class="card-body">
					  <form action="" id="abc" name="myform">
						<input type="text" name="name" placeholder="Please enter the node name" value="" />
						<span>Node type</span><br>
						<select name="type_id" value="">
							<option value="0">Select Node type</option>
							<option value = "1">Play</option>
							<option value='2'>Play TTS</option>
							<option value='3'>Play Menu</option>
							<option value='4'>Play TTS Menu</option>
						</select>
						<input type="hidden" name="key" value=""/>
						<input type="hidden" name="parent" value="${fetchNode.id}" />
						<hr>
						<h3>Required field</h3>
						<input type="text" name="field1" placeholder="Required field1" value=""/>
						<input type="text" name="field2" placeholder="Required field1" value=""/>
						<hr>
						<textarea  name="response" placeholder="Response" value=""></textarea>
					    <input type="submit" class="btn btn-primary" onclick=addBranchNode(${fetchNode.id}) data-dismiss="modal"  value="Save" >
					  </form>
				  </div>
				</div>

	`;
	$(".ivr-node-container").html(html);

}





function save_node_data(nodeId){
	console.log("clicke to save");
	let fetchNode = fetchNodeProps(nodeId);
	let newdata = $("#abc").serializeArray();
	console.log(newdata);
	let prep_data = {};
	$.each(newdata, function(index, value){
		prep_data[value.name] = value.value;
	});
	prep_data['id']=fetchNode.id;
	prep_data['childNode'] = [];
	prep_data['parent'] = fetchNode.parent;
	if(prep_data.parent){ 
		ivr_node[(fetchNode.id)].childNode.push(prep_data);
	}else{
		ivr_node[(fetchNode.id-1)] = prep_data;

	}
	html = compact_node(ivr_node);
	render_node(html);
}

function addBranchNode(nodeId){                
	let newdata = $("#abc").serializeArray();
	console.log(newdata);
	let prep_data = {};
	$.each(newdata, function(index, value){
		// console.log(value, key);
		prep_data[value.name] = value.value;
	});

	
	prep_data['id']= counterId+1;
	prep_data['childNode'] = [];
	counterId++;
	console.log(ivr_node[nodeId-1]);
	ivr_node[nodeId-1]['childNode'].push(prep_data);


	console.log("ivr child node ..");
	// ivr_node.splice(parseInt(prep_data.id), 0, prep_data);
	console.log(ivr_node);
	html = compact_node(ivr_node);
	render_node(html);
}

// function render_tree(){

// }

$('.add-new-node').click(function() {
	$(".ivr-node-container").html(nodeDetail());
});

$(document).on('click','.save-node',function(event) {
	let node_data = {}
	fData= $(this.form ).serializeArray(); 
	$.each(fData, function(index, value) {
		node_data[value.name] = value.value;
	});
	node_data['id'] = counterId+1;
	counterId++;
	node_data['childNode'] = []
	ivr_node.push(node_data);
	node_data = flush();
	event.preventDefault();
	html = compact_node(ivr_node);
	render_node(html);
});


function render_node(html) {
	$(".node-tree").html(html);
}
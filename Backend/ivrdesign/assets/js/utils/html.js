var nodefrm = `
	<div class="card" style="width: 18rem;" id = "node-attr">
		<h3>Node attribute</h3>
	  <div class="card-body">
		  <form action="" id="abc" name="myform">
			<input type="text" name="name" placeholder="Please enter the node name" />
			<span>Node type</span><br>
			<select name="type_id">
				<option value="">Select Node type</option>
				<option value = "1">Play</option>
				<option value='2'>Play TTS</option>
				<option value='3'>Play Menu</option>
				<option value='4'>Play TTS Menu</option>
			</select>
			<input type="hidden" name="key" />
			<hr>
			<h3>Required field</h3>
			<input type="text" name="field1" placeholder="Required field1" />
			<input type="text" name="field2" placeholder="Required field1" />
			<hr>
			<textarea  name="Response" placeholder="Response"></textarea>
		    <input type="submit" class="btn btn-primary save-node" data-dismiss="modal"  value="Save" >

		  </form>
	  </div>
	</div>
`;
"""ivrdesign URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from ivrdesign.apps.ivrnode import views
from django.conf.urls import url
from django.conf.urls.static import static


urlpatterns = [
    url('get_req_field', views.get_required_field),
	url('get_node_type', views.get_noode_type),
    url('save_node', views.save_node_data),
    url('update_node', views.edit_node_data),
    url('remove_node', views.delete_node_data),
    path('admin/', admin.site.urls),
]
